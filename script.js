/*
Циклы в программировании нужны для многократного выполнения подобных операций. Циклы бываюют конечными и бесконечными.
Бесконечным является цикл, условие выхода из которого никогда не выполнится, и цикл будет исполняться вечно. Конечные циклы имеют
определенное количество повторений, и заканчивают свою работу при выполнении условия выхода.
 Примеры циклов: while, for, forEach, for of, for in, do while.
* */

let num = +prompt('Enter a number');
while (num !== parseInt(num)) {
    num = +prompt('Enter an integer');
}

let foundNumbersCounter = 0;

for (let i = 1; i <= num; i++) {
    if (i % 5 === 0) {
        console.log(i);
        foundNumbersCounter++;
    }
}
if (foundNumbersCounter === 0) {
    console.log('Sorry, no numbers')
}

