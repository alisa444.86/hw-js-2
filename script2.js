let m = +prompt('Enter first number');
while (!m || m !== parseInt(m)) {
    alert('Error! Enter an integer');
    m = +prompt('Enter first number');
}
let n = +prompt('Enter second number');
while (!n || n !== parseInt(n)) {
    alert('Error! Enter an integer');
    n = +prompt('Enter second number');
}

nextStep:
    for (m; m <= n; m++) {
        for (let j = 2; j < m; j++) {
            if (m % j === 0) {
                continue nextStep
            }
        }
        console.log(m);
    }
